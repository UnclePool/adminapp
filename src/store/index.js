import Vue from 'vue';
import Vuex from 'vuex';
import deserts from './deserts';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    deserts,
  },
});
