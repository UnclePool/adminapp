import Token from '../services/token/index';

export default {
  state: {
    deserts: [],
    eventItem: {},
  },
  mutations: {
    getEvents(state, payload) {
      state.deserts = payload;
    },
    destroyEvents() {
      return false;
    },
    createEvents(state, payload) {
      state.deserts.push(payload);
    },
    indexEvent(state, payload) {
      state.eventItem = payload;
    },
    editEvent(state, payload) {
      state.eventItem = payload;
    },
  },
  actions: {
    async getEvents({ commit }) {
      try {
        const result = await axios
          .get('https://api-staging.shevet.online/api/v1/events');

        commit('getEvents', result.data);
      } catch (err) {
        console.log(err.message);
      }
    },
    async destroyEvents({ commit }, { id }) {
      try {
        const token = Token.getToken();
        const config = {
          headers: { 'Access-Token': token },
          data: { id },
        };
        const res = await axios
          .delete('https://api-staging.shevet.online/api/v1/events', config);

        commit('destroyEvents', res);
      } catch (error) {
        console.log(error);
        throw error;
      }
    },
    async createEvents({ commit }, event) {
      try {
        const token = Token.getToken();
        const res = await axios
          .post('https://api-staging.shevet.online/api/v1/events', { event }, {
            headers: {
              'Access-Token': token,
            },
          });

        commit('createEvents', res);
      } catch (err) {
        console.log(err);
        throw err;
      }
    },
    async indexEvent({ commit }, { id }) {
      try {
        const token = Token.getToken();
        const res = await axios
          .get(`https://api-staging.shevet.online/api/v1/events/${id}`, {
            headers: { 'Access-Token': token },
          });

        commit('indexEvent', res.data);
      } catch (err) {
        console.log(err.message);
        throw err;
      }
    },
    async editEvent({ commit }, { event, id }) {
      try {
        const token = Token.getToken();
        const res = await axios
          .put(`https://api-staging.shevet.online/api/v1/events/${id}`, { event }, {
            headers: {
              'Access-Token': token,
            },
          });

        commit('editEvent', res.data);
      } catch (e) {
        console.log(e.message);
        throw e;
      }
    },
    authLogout({ commit }) {
      return new Promise((resolve) => {
        commit('authLogout');
        Token.removeToken();
        resolve();
      });
    },
  },
  getters: {
    getDesert(state) {
      return state.deserts;
    },
  },
};
