import Vue from 'vue';
import Router from 'vue-router';
import Admin from './views/Admin.vue';
import Login from './views/Login.vue';
import Token from './services/token/index';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'event',
      component: Admin,
      meta: {
        layout: 'app-layout',
      },
    },
    {
      path: '/add-event',
      name: 'eventItem',
      component: () => import('./views/events/EventsItem.vue'),
      meta: {
        layout: 'app-layout',
      },
    },
    {
      path: '/edit/:id',
      name: 'edit',
      props: true,
      component: () => import('./views/events/EventsItem.vue'),
      meta: {
        layout: 'app-layout',
      },
    },
    {
      path: '/not-main',
      name: 'notMain',
      component: () => import('./views/Some.vue'),
      meta: {
        layout: 'app-layout',
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        public: true,
        layout: 'login-layout',
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = !!Token.getToken();
  const isPublic = to.matched.some(record => record.meta.public);

  if (!isPublic && !isLoggedIn) {
    next({
      path: '/login',
      query: {
        redirect: to.fullPath,
      },
    });
  } else {
    next();
  }
});

export default router;
