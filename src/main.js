import Vue from 'vue';
import router from './router';
import store from './store/index';
import vuetify from './plugins/vuetify';
import './plugins/axios';
import App from './App.vue';

import AppLayout from './layout/AppLayout.vue';
import LoginLayout from './layout/LoginLayout.vue';

Vue.component('app-layout', AppLayout);
Vue.component('login-layout', LoginLayout);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  strict: true,
  render: h => h(App),
}).$mount('#app');
