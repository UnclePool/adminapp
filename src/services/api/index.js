import Vue from 'vue';

// eslint-disable-next-line import/prefer-default-export
export const apiCall = async (method, url, payload) => {
  const storage = method === 'GET' ? 'param' : 'data';
  try {
    const r = await Vue.axios({
      method,
      url,
      [storage]: payload,
    });
    return [null, r.data];
  } catch (e) {
    return [e.response];
  }
};
